import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ForbiddenComponent } from './forbidden.component';

describe('ForbiddenComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ForbiddenComponent
      ],
    }).compileComponents();
  }));

  it('should create the not found component', () => {
    const fixture = TestBed.createComponent(ForbiddenComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });
});
