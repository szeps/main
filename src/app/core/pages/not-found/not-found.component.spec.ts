import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NotFoundComponent } from './not-found.component';

describe('NotFoundComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        NotFoundComponent
      ],
    }).compileComponents();
  }));

  it('should create the not found component', () => {
    const fixture = TestBed.createComponent(NotFoundComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });
});
