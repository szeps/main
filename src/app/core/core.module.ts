import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatListModule } from '@angular/material/list';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { ContentComponent } from './components/content/content.component';
import { HeaderComponent } from './components/header/header.component';
import { AuthGuardService } from './guards/auth.guard';
import { BrowserModule } from '@angular/platform-browser';
import { NotFoundComponent, ForbiddenComponent, HomeComponent } from './pages';

@NgModule({
  imports: [BrowserModule, RouterModule, FlexLayoutModule, MatToolbarModule, MatSidenavModule, MatIconModule, MatButtonModule,
    MatListModule, MatExpansionModule, MatMenuModule],
  declarations: [HeaderComponent, ContentComponent, NotFoundComponent, ForbiddenComponent, HomeComponent],
  exports: [HeaderComponent, ContentComponent, NotFoundComponent, ForbiddenComponent, HomeComponent],
  providers: [AuthGuardService]
})
export class CoreModule { }
