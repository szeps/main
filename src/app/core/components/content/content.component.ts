import { Component } from '@angular/core';

/**
 * Component that renders the sidenav container with the sidenav containing a menu
 * and sidenav content containing the router outlet that renders based on the chosen menu option
 */
@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.scss']
})
export class ContentComponent {

  /**
   * The nav links in the menu.
   *
   */
  navs = [
    { name: 'Projects', icon: 'apps', paths: ['React'] },
    { name: 'Information', icon: 'info', paths: ['CV', 'Contact'] },
    { name: 'Source', icon: 'code', paths: ['GitLab', 'Documentation'] },
    { name: 'Dummy Links', icon: 'link_off', paths: ['Admin', 'Lost', 'Secrets', 'Gone'] }];
}
