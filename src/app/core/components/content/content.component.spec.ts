import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ContentComponent } from './content.component';

describe('ComponentComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ContentComponent
      ],
    }).compileComponents();
  }));

  it('should create the content container', () => {
    const fixture = TestBed.createComponent(ContentComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });
});
