import { NgModule } from '@angular/core';
import { RouterModule, Routes, ActivatedRouteSnapshot, Router } from '@angular/router';
import { AuthGuardService } from '../core/guards/auth.guard';
import { ReactComponent, CVComponent, ContactComponent } from '.';
import { MatCardModule } from '@angular/material/card';
import { MatListModule } from '@angular/material/list';
import { MatTooltipModule } from '@angular/material/tooltip';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';

export const PAGES_ROUTES: Routes = [
  { path: 'react', component: ReactComponent },
  { path: 'cv', component: CVComponent },
  { path: 'contact', component: ContactComponent },
  {
    path: 'documentation',
    children: [],
    resolve: {
      url: 'externalUrlRedirectResolver'
    },
    data: {
      externalUrl: 'https://szeps.gitlab.io/main/'
    }
  },
  {
    path: 'gitlab',
    children: [],
    resolve: {
      url: 'externalUrlRedirectResolver'
    },
    data: {
      externalUrl: 'https://gitlab.com/szeps/'
    }
  },
  { path: 'admin', canActivate: [AuthGuardService], children: [] },
  { path: 'secrets', canActivate: [AuthGuardService], children: [] }
];

@NgModule({
  imports: [MatCardModule, MatListModule, MatTooltipModule, MatIconModule, MatExpansionModule, MatButtonModule, MatProgressSpinnerModule,
    RouterModule.forChild(PAGES_ROUTES)],
  declarations: [ReactComponent, CVComponent, ContactComponent],
  exports: [ReactComponent, CVComponent, ContactComponent],
  providers: [
    {
      provide: 'externalUrlRedirectResolver',
      useValue: (route: ActivatedRouteSnapshot, router: Router) => {
        window.open((route.data as any).externalUrl, '_blank');
        router.navigateByUrl('');
      }
    }
  ]
})
export class PagesModule { }
