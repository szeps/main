import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { CVComponent } from './cv.component';

describe('CVComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        CVComponent
      ],
    }).compileComponents();
  }));

  it('should create the CV component', () => {
    const fixture = TestBed.createComponent(CVComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });
});
