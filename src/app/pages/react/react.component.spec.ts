import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ReactComponent } from './react.component';

describe('ReactComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ReactComponent
      ],
    }).compileComponents();
  }));

  it('should create the React container', () => {
    const fixture = TestBed.createComponent(ReactComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });
});
