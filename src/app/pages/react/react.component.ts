import { Component } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-react',
  templateUrl: './react.component.html',
  styleUrls: ['./react.component.scss']
})
export class ReactComponent {

  public reactUrl;

  constructor(private sanitizer: DomSanitizer) {
    this.reactUrl = sanitizer.bypassSecurityTrustResourceUrl(environment.reactUrl);
  }
}

// time = 0;
// loading = false;
// interval;

// constructor() {
// }

// load() {
//   this.time = 4;
//   clearInterval(this.interval);
//   this.spin();
//   this.interval = setInterval(() => {
//     this.time--;
//     if (this.time === 0) {
//       clearInterval(this.interval);
//       this.reset();
//     }
//   }, 1000);
// }

// spin() {
//   this.loading = true;
// }

// reset() {
//   this.loading = false;
// }

// import { Directive, ElementRef } from '@angular/core';
// import { AnimationBuilder, AnimationPlayer, AnimationMetadata, style, animate } from '@angular/animations';

// @Directive({
//   selector: '[appSpin]'
// })
// export class SpinDirective {
//   player: AnimationPlayer;

//   @Input('appSpin')
//   public set spin(value: boolean) {
//     this.animate(value, this.el.nativeElement);
//   }

//   constructor(private builder: AnimationBuilder, private el: ElementRef) {
//   }

//   private animateSpin(element: any): AnimationMetadata[] {
//     return [
//       style({ top: '*', right: '*' }),
//       animate('1s', style({ top: 0, left: '100px' })),
//       animate('1s', style({ top: '100px', left: '100px' })),
//       animate('1s', style({ top: '100px', left: 0 })),
//       animate('1s', style({ top: 0, left: 0 }))
//     ];
//   }
//   private deAnimateSpin(element: any): AnimationMetadata[] {
//     return [
//       style({ top: '*', right: '*' }),
//       animate('400ms', style({ top: 0, left: 0 })),
//     ];
//   }

//   animate(spin: boolean, element: any) {
//     console.log(element)
//     if (this.player) {
//       this.player.destroy();
//     }
//     let metadata;
//     if (spin) {
//       element.disabled = true;
//       metadata = this.animateSpin(element);
//     } else {
//       element.disabled = false;
//       metadata = this.deAnimateSpin(element);
//     }

//     const factory = this.builder.build(metadata);
//     const player = factory.create(element);

//     player.play();

//     for (let i = 0; i < element.children.length; i++) {
//       this.animate(spin, element.children[i]);
//     }
//   }
// }
