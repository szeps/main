import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { ContactComponent } from './contact.component';

describe('ContactComponent', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        ContactComponent
      ],
    }).compileComponents();
  }));

  it('should create the Contact component', () => {
    const fixture = TestBed.createComponent(ContactComponent);
    const header = fixture.componentInstance;
    expect(header).toBeTruthy();
  });
});
